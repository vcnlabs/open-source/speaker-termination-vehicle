# Speaker Termination Vehicle

This is a forked version of alsa-tools-gui, intended to be capable of disabling the built-in speakers of laptops quickly and unattended. The end goal is a script or executable that can be executed once and will persist even across reboots.

An important distinction must be made between internal speakers and built-in speakers. Internal speakers are beepers generating a set tone, while built-in speakers are the ones that can play audio. It is the latter that we seek to disable.

At the moment this code does not adhere to licensing requirements for the original project. Changes must be described and added to modified files in order to adhere.

This project is intended for Vancouver Community Network internal use for the moment.

#   Requirements:
- sudo apt install gksu

# Done:
- Disable GUI popup
- Identification of built-in speakers

# TODO:
- Write settings to take effect immediately (properly kill off PulseAudio)
- Proper licensing/attribution/changes
- Enable create_firmware_files to persist multiple cards